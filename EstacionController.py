class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class EstacionController(metaclass=SingletonMeta):
    estacion = 21

    def getEstacion(self):
        try:
            if self.estacion == 21 or self.estacion == 0:
                file = open('/home/pi/.rocat/estacion.txt', 'r')
                #file = open('estacion.txt', 'r')
                self.estacion = int(file.read().strip())
                print(f'Leimos del archivo la estacion estacion: {str(self.estacion)}')
            return self.estacion
        except Exception as e:
            return 21

    def setEstacion(self, codigo: str):
        try:
            if 'ñ' in codigo:
                split = codigo.split('ñ')
            else:
                split = codigo.split(':')
            self.estacion = int(split[1])
            file = open('/home/pi/.rocat/estacion.txt', 'w')
            #file = open('estacion.txt', 'w')
            file.write(split[1].strip())
            print(f"Estacion {split[1].strip()} asignada")
            return self.estacion
        except Exception as e:
            print('Error asignando estacion: ' + str(e))
            return 21