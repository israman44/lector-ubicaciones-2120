from PyQt5.QtSql import QSqlDatabase, QSqlQuery

from Ubicacion import Ubicacion

import pandas as pd
from sqlalchemy import create_engine

class DataBase:

    def __init__(self, parent=None):
        super(DataBase, self).__init__()
        self.string_connection = 'mysql+pymysql://rasp:rasp@localhost/rocat'

    def getConnection(self, id):
        name_connection = "database-" + id
        if QSqlDatabase.contains(name_connection):
            QSqlDatabase.removeDatabase(name_connection)
        db = QSqlDatabase.addDatabase('QMYSQL', name_connection)
        db.setHostName("localhost")
        db.setDatabaseName("rocat")
        db.setUserName("rasp")
        db.setPassword("rasp")
        return db

    def get_estacion_saved(self):

        db_connection_str = self.string_connection
        db_connection = create_engine(db_connection_str)

        df = pd.read_sql(f"""SELECT UUID,CODIGO, ESTACION, FECHA, ENVIADO FROM REGISTROS WHERE ENVIADO=0""", con=db_connection)

        return df

    def save_estacion(self, ubicacion: Ubicacion):
        try:
            db_connection = create_engine(self.string_connection)
            sql = f"""INSERT INTO REGISTROS (UUID, CODIGO, ESTACION, FECHA, ENVIADO) VALUES ('{ubicacion.uuid}', '{ubicacion.codigo}', {ubicacion.estacion}, 
            '{ubicacion.fecha}', 0);"""
            print("OK")
            db_connection.execute(sql)
            db_connection.dispose()
            print("Se guardo el codigo correctamente")
            return True
        except Exception as e:
            print("Error guardar codigo: " + str(e))
            return False

    def remove_pendiente(self, ubicacion):
        try:
            db_connection = create_engine(self.string_connection)
            sql = f"""DELETE FROM REGISTROS WHERE UUID='{ubicacion.uuid}';"""
            db_connection.execute(sql)
            db_connection.dispose()
            print("Se eliminó la ubicacion")
            return True
        except Exception as e:
            print("Error al eliminar la ubicacion: " + str(e))
            return False

    def remove_pendiente_by_uuid(self, uuid):
        try:
            db_connection = create_engine(self.string_connection)
            sql = f"""DELETE FROM REGISTROS WHERE UUID='{uuid}';"""
            db_connection.execute(sql)
            db_connection.dispose()
            print("Se eliminó la ubicacion")
            return True
        except Exception as e:
            print("Error al eliminar la ubicacion: " + str(e))
            return False
