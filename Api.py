import json
import requests
from pandas import DataFrame

from DataBase import DataBase
from Helper import validateResponse


class Api:
    def __init__(self, codigos_pendientes: DataFrame):
        super(Api, self).__init__()
        self.codigos_pendientes = codigos_pendientes

    def run(self):
        # ubicaciones = []

        print("1")
        ubicaciones = self.codigos_pendientes.to_dict(orient='records')
        print("2")
        print(ubicaciones)

        #
        # for ubicacion in self.codigos_pendientes:
        #     print('Ubicacion')
        #     print(ubicacion)
        #     print('type')
        #     print(type)
        #     ubicaciones.append(ubicacion.getDict())


        payload = {
            '_ubicaciones': json.dumps(ubicaciones)
        }
        try:
            response = requests.post('https://servicios.erprocataxidermy.com/barcode/registrar/multiple', data=payload, timeout=(6, 6))
            data = validateResponse(response)
            print('DATA')
            print(data)
            self.codigos_pendientes  = self.delete_correct_ubicaciones(data['ubicacionesHistorial'])
            return self.codigos_pendientes
            # TODO eliminar de la lista los que estan correctos
        except Exception as e:
            print("EnviarCodigo Exception: {}".format(str(e)))
            return self.codigos_pendientes
            #self.set_save_in_local()


    def delete_correct_ubicaciones(self, correctos):
        self.codigos_pendientes = self.codigos_pendientes[~self.codigos_pendientes['UUID'].isin(correctos)]

        for correto in correctos:
            db = DataBase()
            db.remove_pendiente_by_uuid(correto)

        print("QUedan")
        print(self.codigos_pendientes)
        return self.codigos_pendientes

        # new_codigos_pendientes = []
        # print("CODIGOS PENDIENTES")
        # print(self.codigos_pendientes)
        # print(correctos)
        # print(type(correctos))
        # for x in self.codigos_pendientes.iterrows():
        #     print("X")
        #     print(x.)
        #     if not x.uuid in correctos:
        #         print(f"No se guardo {x.uuid}")
        #         new_codigos_pendientes.append(x)
        #     else:
        #         if x.saved_in_local:
        #
        #             print('Remove from local')
        #             db = DataBase()
        #             db.remove_pendiente(x)
        #
        # print("NEW CODIGOS PENDIENTES")
        # print(new_codigos_pendientes)

        #self.codigos_pendientes[:] = [x for x in self.codigos_pendientes if not x.uuid in correctos]

    def set_save_in_local(self):
        print('guardar local')
        db = DataBase()
        for ubicacion in self.codigos_pendientes:
            if not ubicacion.saved_in_local:
                ubicacion.saved_in_local = db.save_estacion(ubicacion)