import re
import sys
#from pynput import keyboard
from KeyboardListen import listen_keyboard
print(sys.version)

import pandas as pd

from Api import Api
from DataBase import DataBase
from Ubicacion import Ubicacion
from EstacionController import EstacionController
import os



codigo_pendientes = DataBase().get_estacion_saved()
counter = 0
EstacionController().getEstacion()

print(f"Estacion: {EstacionController().estacion}")

print(codigo_pendientes)

print(type(codigo_pendientes))

def add_ubicacion(ubicacion: Ubicacion):
    db = DataBase()
    ubicacion.saved_in_local = db.save_estacion(ubicacion)
    global codigo_pendientes
    new_ubicacion = pd.DataFrame({
        'CODIGO': [ubicacion.codigo],
        'ESTACION': [ubicacion.estacion],
        'FECHA': [ubicacion.fecha],
        'ENVIADO': [ubicacion.enviado],
        'UUID': [ubicacion.uuid]
    })

    codigo_pendientes = pd.concat([codigo_pendientes, new_ubicacion])
    print(codigo_pendientes)


def process_pendientes():
    global codigo_pendientes
    codigo_pendientes = Api(codigo_pendientes).run()
    #print('procesar: ')
    #print('\n'.join('{}: {}'.format(*k) for k in enumerate(codigo_pendientes)))
    print('Pendientes')
    print(codigo_pendientes)


def process_coode(codigo: str):
    codigo = re.sub(r"\s+", "", codigo)

    if 'Estacion' in codigo or 'estacion' in codigo:
        print("Asignando estacion")
        EstacionController().setEstacion(codigo)
    else:
        if 'ID:' in codigo or 'id:' in codigo:
            split = codigo.split('P:')
            codigo = 'P:' + split[1]
            ubicacion = Ubicacion(codigo)
            add_ubicacion(ubicacion)
            #codigo_pendientes.append(Ubicacion(codigo))
            #process_pendientes(codigo)
        elif 'i-' in codigo:
            ubicacion = Ubicacion(codigo)
            add_ubicacion(ubicacion)
        else:
            print('Codigo de barras incompatible')

            return

        process_pendientes()

codigo = ''

def on_press(key):
    pass

def on_release(key):
    global codigo, counter
    # print('{0} released'.format(
    #     key))
    # if hasattr(key, 'char') and key.char == 'i':
    #     print("Se presiono la i")
    # if key == keyboard.Key.esc:
    #     return False

    if key == 'enter':
        counter += 1
        codigo = codigo.replace('.', '')
        print(f"Codigo: {codigo}")
        archivo = open("datos.txt", "w")
        archivo.write(f"{codigo}\n")
        process_coode(codigo)
        codigo = ''
        codigo = ''
        if counter == 20:
            os.system('clear')
        return

    if isinstance(key, str):
        codigo += key



while True:
    try:
        # Collect events until released
        # with keyboard.Listener(
        #         on_press=on_press,
        #         on_release=on_release) as listener:
        #     listener.join()

        listen_keyboard(
            on_press=on_press,
            on_release=on_release,
            delay_second_char=0.00,
            delay_other_chars=0.00
        )
    except Exception as e:
        print(f"Error: {e}")
        codigo = ''

# while True:
#     counter += 1
#     print("""
#     ===============================
#             Nueva Lectura
#     ===============================
#     """)
#     try:
#         print(end="")
#         os.system('clear')
#         codigo = input()
#         print(f"Se leyo: {codigo}")
#         process_coode(codigo)
#         if counter == 20:
#             os.system('clear')
#     except EOFError as e:
#         print("Catch error eof")
#         print(end="")



"""
create database 
GRANT ALL PRIVILEGES ON *.* TO 'rasp'@'localhost' IDENTIFIED BY 'rasp';


CREATE TABLE IF NOT EXISTS REGISTROS (
    ID_REGISTRO int(11) NOT NULL AUTO_INCREMENT,
    UUID varchar(100) Not Null,
    CODIGO varchar(100) DEFAULT NULL,
    ESTACION int,
    ENVIADO int NOT NULL,
    FECHA varchar(30) DEFAULT NULL,
    PRIMARY KEY (ID_REGISTRO)
  );
  
  
"""