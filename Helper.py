import threading


def synchronized(func):
    func.__lock__ = threading.Lock()

    def synced_func(*args, **kws):
        with func.__lock__:
            return func(*args, **kws)

    return synced_func

@synchronized
def validateResponse(response):
    response.raise_for_status()
    print("RESPUESTA")
    try:
        parsed_response = response.json()
        print(parsed_response)
        success = parsed_response.get("success")
        sql_code = parsed_response.get("sqlCode")
        message = parsed_response.get("mensaje")
        if success == '1' and sql_code == '200':
            data = parsed_response.get("data")
            return data
    except Exception as e:
        print("Si entra al try catch")
        raise Exception('Error: {}'.format(e))
    raise Exception('Error: {}'.format(message))