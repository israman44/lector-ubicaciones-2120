import datetime
import uuid

from EstacionController import EstacionController


class Ubicacion:
    uuid: str
    codigo: str
    fecha:str
    estacion: int
    saved_in_local: bool

    def __init__(self, codigo):
        self.estacion = EstacionController().getEstacion()
        self.fecha = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.codigo_antiguo = codigo
        self.codigo = '0'
        self.enviado = 0
        self.set_codigo()
        self.saved_in_local = False
        self.uuid = str(uuid.uuid4())

    def set_codigo(self):
        if 'i-' in self.codigo_antiguo:
            split = self.codigo_antiguo.split('-')
            if len(split) == 2:
                self.codigo = split[1]
        if 'P:' in self.codigo_antiguo:
            split = self.codigo_antiguo.split(':')
            if len(split) == 6:
                self.codigo = split[5]


    def getDict(self):
        return {
            '_codigo': self.codigo,
            '_fecha': self.fecha,
            '_estacion': self.estacion,
            '_uuid': str(self.uuid)
        }

    def __str__(self):
        return """{}, {}, {}, {}""".format(self.codigo, self.fecha, self.estacion, self.saved_in_local)


