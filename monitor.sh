#!/bin/bash
exit_code=-1
until [ $exit_code = 0 ]; do
    python3 /home/pi/.rocat/lector.py
    exit_code=$?
    echo "Server 'myserver' crashed with exit code $exit_code.  .." >&2
    sleep 2
done